-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 12, 2015 at 03:31 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paytooth`
--

-- --------------------------------------------------------

--
-- Table structure for table `pt_transactions`
--

CREATE TABLE `pt_transactions` (
  `pt_id_transaction` bigint(50) NOT NULL,
  `pt_id_payer` bigint(50) NOT NULL,
  `pt_id_receiver` bigint(50) NOT NULL,
  `pt_amount` float NOT NULL,
  `pt_currency` varchar(5) NOT NULL,
  `pt_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pt_transactions`
--

INSERT INTO `pt_transactions` (`pt_id_transaction`, `pt_id_payer`, `pt_id_receiver`, `pt_amount`, `pt_currency`, `pt_timestamp`) VALUES
(1, 2, 3, 12.34, 'USD', '2015-09-08 20:33:34'),
(2, 3, 2, 52.34, 'USD', '2015-09-08 20:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `pt_users`
--

CREATE TABLE `pt_users` (
  `pt_id_user` bigint(50) NOT NULL,
  `pt_name` varchar(50) NOT NULL,
  `pt_email` varchar(50) NOT NULL,
  `pt_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pt_users`
--

INSERT INTO `pt_users` (`pt_id_user`, `pt_name`, `pt_email`, `pt_timestamp`) VALUES
(2, '', 'gybra@hotmail.it', '2015-09-08 20:28:22'),
(3, 'Oreste', 'oresteacacia@gmail.com', '2015-09-09 07:31:08'),
(4, 'oreste', 'fhjd@fjjf.com', '2015-09-12 07:55:35'),
(5, 'Oreste23', 'oresteacacia@gmail2.com', '2015-09-12 13:26:11');

-- --------------------------------------------------------

--
-- Table structure for table `pt_users_stuff`
--

CREATE TABLE `pt_users_stuff` (
  `pt_id_user_stuff` bigint(50) NOT NULL,
  `pt_id_user` bigint(50) NOT NULL,
  `pt_mac_address` varchar(50) NOT NULL,
  `pt_gcm` mediumtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pt_users_stuff`
--

INSERT INTO `pt_users_stuff` (`pt_id_user_stuff`, `pt_id_user`, `pt_mac_address`, `pt_gcm`) VALUES
(1, 2, '00:dd:11', 'new token3'),
(2, 3, '00:44:44', 'tokenneneee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pt_transactions`
--
ALTER TABLE `pt_transactions`
  ADD PRIMARY KEY (`pt_id_transaction`),
  ADD KEY `pt_id_payer` (`pt_id_payer`),
  ADD KEY `pt_id_receiver` (`pt_id_receiver`),
  ADD KEY `pt_currency` (`pt_currency`),
  ADD KEY `pt_timestamp` (`pt_timestamp`);

--
-- Indexes for table `pt_users`
--
ALTER TABLE `pt_users`
  ADD PRIMARY KEY (`pt_id_user`),
  ADD KEY `pt_timestamp` (`pt_timestamp`),
  ADD KEY `pt_email` (`pt_email`);

--
-- Indexes for table `pt_users_stuff`
--
ALTER TABLE `pt_users_stuff`
  ADD PRIMARY KEY (`pt_id_user_stuff`),
  ADD KEY `pt_id_user` (`pt_id_user`),
  ADD KEY `pt_mac_address` (`pt_mac_address`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pt_transactions`
--
ALTER TABLE `pt_transactions`
  MODIFY `pt_id_transaction` bigint(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pt_users`
--
ALTER TABLE `pt_users`
  MODIFY `pt_id_user` bigint(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pt_users_stuff`
--
ALTER TABLE `pt_users_stuff`
  MODIFY `pt_id_user_stuff` bigint(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
