<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/user/register', 'UserController@register');
Route::post('/user/encrypt', 'UserController@encrypt');


/*
--- PAY ROUTING ---
*/

Route::post('/pay', 'PayController@pay');
Route::post('/pay/users', 'PayController@getUsers');
Route::get('/pay/{mac_address}/{page?}', 'PayController@getTransactions');