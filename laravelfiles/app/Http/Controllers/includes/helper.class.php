<?php

require_once 'Encryption/Crypt/RSA.php';

/**
* helper class for Controllers which is helpful to check params passed over a controlelr etc.
*/
class Helper{
	/**
	* checks if the params are passed correctly
	*/
	public function isOkParams($post,$should_indexes){
		$counter = 0;
		foreach ($post as $key => $value) {
			if(in_array($key, $should_indexes)){
				$counter++;
			}
		}
		return $counter===count($should_indexes) ? array('response'=>true) : $this->errorJSON(); 
	}

	/**
	* print an error JSON related to the arguments passed
	*/
	public function errorJSON(){
		$response = response()->json(["status"=>"700","description"=>"not all arguments passed"]);
		return array('response' => false, "json"=>$response);
	}

	/**
	* print a generic error JSON
	*/
	public function genericErrorJSON(){
		return response()->json(["status"=>"700","description"=>"error"]);
	}

	/**
     *
     * @return json string decoded and decrypted with the RSA private key
     */
    public function decrypt($text=null){
        $rsa = new Crypt_RSA;
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $rsa->loadKey($this->rsaPrivateKey());
        return json_decode($rsa->decrypt(base64_decode($text===null ? file_get_contents('php://input') : $text)));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function encrypt($text=null)
    {
        $rsa = new Crypt_RSA;
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $rsa->loadKey($this->rsaPublicKey());
        return base64_encode($rsa->encrypt($text===null ? file_get_contents('php://input') : $text));
    }

    /**
     * @return the RSA public key
     */
    private function rsaPublicKey(){
        return "-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAyl4syZiCDS4F/MygAew8
lL66ut3cDlBPHPAl7lQFCUIe+KG+0PNRZ5GOBpUd+T2/WdJID8qik600ktSyx55X
ZChi5qs59Txu6oa2YmAVuZXPWi4oAouoomNq4KWfQsCB9etqWOV7Y/IdUGHJGNo7
U1IrNHFs3oKYDhr728ifLq31LhwCUM+LDk6LsvzXQbf0OCUYqzPxRlR9g9n+VqoC
JR1lyrW8BOsnoqpldzNVnLbe53aJKBSrPZw052b7KqcsWVpq3BiZLY76okADLYzA
nwsoHE/TWOFaQ6uuQLVnpY5dgvY+ltt7xe9lK+fEF1Kv83zLK7dF0k4vEOtKENEx
GFo9OZGlQTwnDBHu+t7f1ITE8/Du4uUMEZZouJ9SxhMxbMP7YUFywKMFaXElPy/h
lsg+q0AKcSlrVAwOp0G4TAXLnq8Ki4NkkLTr+IqjQ4kTS4LDep/aHbaPasiR3Ivl
uALjkcCs+nIzeIp9C1CqceTT97f4dQGIqxgxLE+BKOEU5LkTbRYnqu/qNNm0cKaF
ryH1HaXLg6w9RWP0m94cznVE1ArrJyEEuQnfcYbufbh+n47Iyl8Nl/VTR0rWdZtn
zXoe8a0xCsFXv+nnT7DL4AmfSj/3c0egyK/W1/44nqSHZTDFPGSLgPgG+NUBVnnn
sXlLZlRlkZqLWCD5/kvn6X8CAwEAAQ==
-----END PUBLIC KEY-----";
    }

    /**
     * @return the RSA private key
     */
    private function rsaPrivateKey(){
        return "-----BEGIN RSA PRIVATE KEY-----
MIIJKwIBAAKCAgEAyl4syZiCDS4F/MygAew8lL66ut3cDlBPHPAl7lQFCUIe+KG+
0PNRZ5GOBpUd+T2/WdJID8qik600ktSyx55XZChi5qs59Txu6oa2YmAVuZXPWi4o
AouoomNq4KWfQsCB9etqWOV7Y/IdUGHJGNo7U1IrNHFs3oKYDhr728ifLq31LhwC
UM+LDk6LsvzXQbf0OCUYqzPxRlR9g9n+VqoCJR1lyrW8BOsnoqpldzNVnLbe53aJ
KBSrPZw052b7KqcsWVpq3BiZLY76okADLYzAnwsoHE/TWOFaQ6uuQLVnpY5dgvY+
ltt7xe9lK+fEF1Kv83zLK7dF0k4vEOtKENExGFo9OZGlQTwnDBHu+t7f1ITE8/Du
4uUMEZZouJ9SxhMxbMP7YUFywKMFaXElPy/hlsg+q0AKcSlrVAwOp0G4TAXLnq8K
i4NkkLTr+IqjQ4kTS4LDep/aHbaPasiR3IvluALjkcCs+nIzeIp9C1CqceTT97f4
dQGIqxgxLE+BKOEU5LkTbRYnqu/qNNm0cKaFryH1HaXLg6w9RWP0m94cznVE1Arr
JyEEuQnfcYbufbh+n47Iyl8Nl/VTR0rWdZtnzXoe8a0xCsFXv+nnT7DL4AmfSj/3
c0egyK/W1/44nqSHZTDFPGSLgPgG+NUBVnnnsXlLZlRlkZqLWCD5/kvn6X8CAwEA
AQKCAgEAtMGNq7wHXhJf0sBLo6wJKKt8SvFkX3yoTabLmqd9oBlpZJfEwwqzCZlf
jM2g6kGXK3nCWCidMt8g+I336omqcZIJVRRYPo5uXBlby18byJrhjSFxJ2TribDX
hDUrngopiylt4l2H62wVdvkBEKvNFc1tPfiaT93CR8cec+AtHxrOxxLJlrjq8iqC
aauh8deH0qZ/9Fv/BNzvB9itb2eHZvNtOrU9bf9FdexRmTlHHSXS0Fh2hlB/7BYc
wxye+Cg+WsmCbXmjSKhPgqE+tW7llqn2d+NtWhRf9dXhXXcrTxtst0+P9FuzoMcG
TUNFSbfX5drv9upK+WBQdgJUM9VGzWzhGuMv9SFf7Dpi4mNKinx4o/EsWV4PnQkS
inICrTuShClsAFqql3xeGLrscQgkugAZvh7QIgGQvCZw+4NPhzCi6xI8myr1JTxX
3sxzYJrVMXxfNbd1zOYeNes0ZQbOCiRVsDlQzp2eKY/p0s8sROD2Eb4IV46c0pOf
B+4u2rjqP2Tcvv8/AYRhrX+AbaOiJvE8GkZzmg1nCmY1QLvCZRX27d6fFCGOMJDU
hklLiBE54dqeyMpXhIAbuwg55RWz+u+cub1F5c42wHBgPM+L9Eji0tRoqrGJccyO
oZcSRfTI0XBli4v1p1tCW+Pi3NAnS8ohnrvvwtlBh1+COMZsDeECggEBAOu0+AIG
ROehq7psjjk/7JscgiYic+p3e0YxMDncIyqvmMdg3j3fpRZDHEIqWkyxPHR3WtkZ
Ou+H3+ORhVzue58Fn6KjMuT5O9lRDEm3xGGV8NGX7gbfY3pN5J6WbW0YUPCZDFvV
NeVI4HegZchkaesWTtzEUD1/TGKa+s1rN1lYP7342kKp9kVuh/DzvK6SeiPsxIUb
YWi56E2h708VPhnUD8a+ZHGi2BquOkABtQfL60fGLuXE2NUTPbmEwq9BCPVfToPC
+OsSaplgM/9tGsMyyBXtu/Zzj7EHlyy44aSR1FZrNfCEhlPq5sfw9DWUKlsI7hhT
B8r2EczvFnJ3q4kCggEBANvKaBRB4NWPxNQZDW6qpcSBwdK0P0hZgAQAjgAaxpEC
OinAhlxmRcqAhfqBRB3GELamTdHJrtmNQjgxRkuQOafUPtuHqH5NCGy/bPKRgjA4
MdSd0ADqZ6/Ksc+1aK+5+PynMjF4bEeEwzyy6gNAtE9xd1VY+gs9Zb1R/ZD+t7nV
FKjVgxvZtxCuIl5yA9COkVlMG+0jD2AwH92EKEpWtPWj858gBblRIO+60auWtEFU
ZSwYNHP9wDQTzTMmlzTq001Fc8PJ+0DPGIfPp6MXtxNvmOHSF7GCbvp8qOcIiX7/
hLCnK1AeKQCEZuY5+F5+xUFqCyjYdVtyLmJ8Gd4WgscCggEBAJ1oDy+a7C0bxEkM
8bW9wM6dLTU2UN7Fx+dKNgCNyFZbSdXsFL6ROfgx/Q8apY8N1WeZxWux86++t3+L
0NoM0TWTVdYQ0o4i+xrXizM3q8fXkCkIIwcn65Djr8dQgecXFAZ4zs4NBp8GGhEJ
Zq96j1lVXnkECOnotyQqh07jSOU7j+XX6DoYssQPWm1I+DpEy5huJ5cUf+qmLEKb
3+qaLwykl5yLzRwuOc8nhvLzsOdyAL+/09XRrVQuM0xPIoxTDB6A9Tf83qptdA7V
NiRfwj2VDhXG231sEwuQHB7boKR8VqcXlb/3YJVuC4x+lzeoqNh+kDJiUJPz3FNV
X1I2JLECggEBAJhm0iJxx7ft97pmILVC6KzImKBE0biWLrkXtV8HMfasx63ZzJDX
bb9LvRsFdTj79OzhhgZYxcAePfN+31JYEFJ6TpekikYBnjzhw2azVMWgwZS/VNK9
tuXjM8Zl8NAgsTBnstswuEwjF8nH+ZIF1MFdWTS5/K+8hAZxitR2eukFgkzLuByg
q1qKlFRO1KxRnyF/L8IP1IKK/B16nQFm/BVz+OK2rqFgSulP7kD/v7+pCHo50m03
X4X80QWN1b0gsyHabbouHOVEdKr/Zyskr5JviF/VcQ5p9hAETo3ckpKYFOb1E4h7
UU0FgeBUBA9p8dc488H/zeh51QX/LWSc0VMCggEBAKbMolSR8JS5ukYgqb0HrPs7
7MhAtZrWCPYxygcTIVxyMkgFJVUSXqM6+fV+GaBr8byoiBHZMxvcwmo1zllcVjM1
K9M3QVQhmRjt6BHup8nf+m1uVYHnmO0cDo6zo73iROiK/dUvdF3wVMXxabWqJz/u
kSeNT106JgN4SRKCSaDh/hC9WMEYr7HkB6dFNv/Rt0mpwql+Q2k7V6nxqNeCcDRB
SBOYwbGYYLbw+E7MStTfcY0tKyU5TttvWML9pdxx24M03YQk6uyzyjeYUFrs51JW
Z5Yh+XfApAXLLeGCxnKOEJL/zhvZaFP/m0m7SSQNYdFrNisTvK+Nsa1xaeZIqt8=
-----END RSA PRIVATE KEY-----";
    }
}