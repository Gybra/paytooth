<?php

namespace App\Http\Controllers;

require_once 'includes/helper.class.php';

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Helper;

class PayController extends Controller
{
    /**
     * make a payment
     *
     * @return Response
     */
    public function pay()
    {
        set_time_limit(0);

        $helper = new Helper();

        $json = $helper->decrypt();

        $response = $helper->isOkParams($json,array("receiver","payer","amount","currency"));

        if($response["response"]){
            $results = DB::select(DB::raw("select pt_id_user,pt_gcm from pt_users left outer join pt_users_stuff on pt_users.pt_id_user = pt_users_stuff.pt_id_user where pt_mac_address in (:mac_receiver,:mac_payer) order by FIELD(pt_mac_address,:payer,:receiver)"),array("mac_receiver"=>$json->receiver,"mac_payer"=>$json->payer,"payer"=>$json->payer,"receiver"=>$json->receiver));
            if(count($results)==2){
                DB::insert(DB::raw("insert into pt_transactions (pt_id_payer,pt_id_receiver,pt_amount,pt_currency) values (:payer,:receiver,:amount,:currency)"),array("payer"=>$results[0]->pt_id_user,"receiver"=>$results[1]->pt_id_user,"amount"=>$json->amount,"currency"=>$json->currency));

                return response()->json(["status"=>"200","description"=>"transaction registered"]);
            }

            return response()->json(["status"=>"700","description"=>"payer or receiver is not registered on the system"]);
        }

        return $response["json"];

        // {"receiver":"00:44:44","payer":"00:dd:11","amount":52.34,"currency":"USD"}
    }

    /**
    * @return the users registered on the system by matching the mac address of each user
    */
    public function getUsers()
    {
        set_time_limit(0);

        $helper = new Helper();

        $json = $helper->decrypt();

        $response = $helper->isOkParams($json,array("mac"));

        if($response["response"]){
            $macs = explode(",", $json->mac);
            $final_mac = array();
            $str_mac = "";
            $index = 1;
            foreach ($macs as $mac) {
                $final_mac["mac".$index] = $mac;
                $str_mac .= ":mac".$index++.",";
            }
            $str_mac = substr($str_mac, 0, strlen($str_mac)-1);
            $results = DB::select(DB::raw("select pt_mac_address,pt_email,pt_name from pt_users left outer join pt_users_stuff on pt_users.pt_id_user = pt_users_stuff.pt_id_user where pt_mac_address in (".$str_mac.")"),$final_mac);
            $json = array();
            $index = 0;
            foreach ($results as $row) {
                $json[$index]["email"] = $row->pt_email;
                $json[$index]["name"] = $row->pt_name;
                $json[$index++]["mac"] = $row->pt_mac_address;
            }

            return $helper->encrypt(response()->json(["status"=>"200","content"=>$json]));
        }

        //{"mac":"00:44:44,00:dd:11"}

        return $response["json"];
    }

    /**
     * @return all the transactions made by an email address
     */
    public function getTransactions($mac_address,$page=null)
    {
        set_time_limit(0);

        $results = DB::select(DB::raw("select pt_users_stuff.pt_id_user from pt_users left outer join pt_users_stuff on pt_users.pt_id_user = pt_users_stuff.pt_id_user where pt_mac_address=:mac"),array("mac"=>$mac_address));
        $helper = new Helper();

        if(count($results)>0){
        	$id_user = $results[0]->pt_id_user;
        	$results = DB::select(DB::raw("select pt_id_payer, pt_amount, pt_currency, pt_transactions.pt_timestamp, users.pt_name payer, users2.pt_name receiver from (pt_users users left outer join pt_transactions on users.pt_id_user = pt_transactions.pt_id_payer) left outer join pt_users users2 on users2.pt_id_user = pt_transactions.pt_id_receiver where pt_id_payer = :id_payer or pt_id_receiver = :id_receiver order by pt_transactions.pt_timestamp desc"),array("id_payer"=>$id_user,"id_receiver"=>$id_user));

        	$json = array();
        	$index = 0;
        	/*foreach ($results as $row) {
        		$json[$index]["name"] = $row->pt_id_payer===$id_user ? $row->receiver : $row->payer;
        		$json[$index]["amount"] = $row->pt_amount;
        		$json[$index]["currency"] = $row->pt_currency;
        		$json[$index]["is_payer"] = $row->pt_id_payer===$id_user ? true : false;
        		$json[$index++]["timestamp"] = $row->pt_timestamp;
        	}*/

            for($i=($page==null ? 0 : ($page*30)-30);$i<($page==null ? 30 : $page*30);$i++){
                $json[$index]["name"] = "Oreste";
                $json[$index]["amount"] = (52.34)+$i;
                $json[$index]["currency"] = "USD";
                $json[$index]["is_payer"] =  $i%2==0;
                $json[$index++]["timestamp"] = "2015-09-08 22:35:35";
            }

        	return response()->json(["status"=>"200","content"=>$json]);
        }

        return $helper->genericErrorJSON();
    }
}
