<?php

namespace App\Http\Controllers;

require_once 'includes/helper.class.php';


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Helper;
use DB;

class UserController extends Controller
{
    /**
     * registers a new user to the system
     *
     * @return Response
     */
    public function register()
    {   
        set_time_limit(0);

        $helper = new Helper();

        $json = $helper->decrypt();

        $response = $helper->isOkParams($json,array("name","email","mac","gcm"));

        if($response["response"]){
            if(count(DB::select(DB::raw("select * from pt_users left outer join pt_users_stuff on pt_users.pt_id_user = pt_users_stuff.pt_id_user where pt_email=:email and pt_mac_address=:mac"),array("mac"=>$json->mac,"email"=>$json->email)))>0){
                DB::update(DB::raw("update pt_users left outer join pt_users_stuff on pt_users.pt_id_user = pt_users_stuff.pt_id_user set pt_gcm=:gcm, pt_name=:name where pt_mac_address=:mac"),array("mac"=>$json->mac,"gcm"=>$json->gcm,"name"=>$json->name));
                return response()->json(["status"=>"200","description"=>"user registered"]);
            }

            $results = DB::select(DB::raw("select * from pt_users where pt_email=:email"),array("email"=>$json->email));
            if(count($results)>0){
            	DB::insert(DB::raw("insert into pt_users_stuff (pt_id_user,pt_mac_address,pt_gcm) values (:id,:mac,:gcm)"),array("id"=>$results[0]->pt_id_user,"mac"=>$json->mac,"gcm"=>$json->gcm));
                return response()->json(["status"=>"200","description"=>"user registered"]);
            }
            
            if(DB::insert(DB::raw("insert into pt_users (pt_name,pt_email) values (:name,:email)"),array("email"=>$json->email,"name"=>$json->name))){
                $last_id = DB::getPdo()->lastInsertId();
                DB::insert(DB::raw("insert into pt_users_stuff (pt_id_user,pt_mac_address,pt_gcm) values (:id,:mac,:gcm)"),array("id"=>$last_id,"mac"=>$json->mac,"gcm"=>$json->gcm));
                return response()->json(["status"=>"200","description"=>"user registered"]);
            }
                
            return $helper->genericErrorJSON();
        }

        return $response["json"];
        //{"name":"Oreste","email":"oresteacacia@gmail.com","mac":"00:dd:22","gcm":"new token23"}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function encrypt()
    {
        return (new Helper())->encrypt();
    }
}
